package nhan.micronaunt.demo.validator;

import io.micronaut.core.annotation.AnnotationValue;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.validation.validator.constraints.ConstraintValidator;
import io.micronaut.validation.validator.constraints.ConstraintValidatorContext;
import jakarta.inject.Singleton;
import org.apache.commons.lang3.StringUtils;

@Singleton
@Introspected
public class MaxStringValidator implements ConstraintValidator<MaxStringConstraint, String> {

    @Override
    public boolean isValid(@Nullable String value, @NonNull AnnotationValue<MaxStringConstraint> annotationMetadata, @NonNull ConstraintValidatorContext context) {
        int maxValue = annotationMetadata.get("maxValue", Integer.class).orElse(0);
        context.messageTemplate("invalid ({validatedValue}), Only " + maxValue + " characters are accepted");
        return !(StringUtils.isEmpty(value) || value.length() > maxValue);
    }
}
