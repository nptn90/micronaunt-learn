package nhan.micronaunt.demo.services;

import jakarta.inject.Singleton;
import nhan.micronaunt.demo.entity.Owner;
import nhan.micronaunt.demo.entity.Symbol;
import nhan.micronaunt.demo.request.OwnerRequest;
import nhan.micronaunt.demo.response.OwnerResponse;
import nhan.micronaunt.demo.response.OwnerResponseConverter;
import nhan.micronaunt.demo.store.InMemoryStore;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class OwnerService {

    private final OwnerResponseConverter ownerResponseConverter;
    private final InMemoryStore inMemoryStore;

    public OwnerService(OwnerResponseConverter ownerResponseConverter, InMemoryStore inMemoryStore) {
        this.ownerResponseConverter = ownerResponseConverter;
        this.inMemoryStore = inMemoryStore;
    }

    public List<OwnerResponse> toResponseList(List<Owner> listEntity) {
        return listEntity
                .stream()
                .map(ownerResponseConverter::convert)
                .collect(Collectors.toList());
    }

    public List<OwnerResponse> toResponseList(List<Owner> listEntity, String ownerName) {
        return listEntity
                .stream()
                .filter(owner -> owner.name().equals(ownerName))
                .map(ownerResponseConverter::convert)
                .collect(Collectors.toList());
    }

    public OwnerResponse addOwner(OwnerRequest ownerRequest) {
        Symbol symbol = inMemoryStore.getMapSymbols().get(ownerRequest.symbolId());
        if (symbol == null) {
            throw new RuntimeException("Cannot find Symbol");
        }

        int remain = symbol.number() - symbol.owners().size();

        if (ownerRequest.holdNumber() > remain) {
            throw new RuntimeException("There's no space to add");
        }

        Owner owner = new Owner(ownerRequest);
        symbol.owners().add(owner);
        inMemoryStore.getMapSymbols().put(symbol.value(), symbol);
        return ownerResponseConverter.convert(owner);
    }
}
