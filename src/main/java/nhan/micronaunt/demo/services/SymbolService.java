package nhan.micronaunt.demo.services;

import jakarta.inject.Singleton;
import nhan.micronaunt.demo.entity.Symbol;
import nhan.micronaunt.demo.response.SymbolResponse;
import nhan.micronaunt.demo.response.SymbolResponseConverter;
import nhan.micronaunt.demo.store.InMemoryStore;

import java.util.List;

@Singleton
public class SymbolService {
    private final InMemoryStore inMemoryStore;
    private final SymbolResponseConverter symbolResponseConverter;

    public SymbolService(InMemoryStore inMemoryStore, SymbolResponseConverter symbolResponseConverter) {
        this.inMemoryStore = inMemoryStore;
        this.symbolResponseConverter = symbolResponseConverter;
    }

    public List<SymbolResponse> getAllSymbolsResponse() {
        return symbolResponseConverter.toResponseList(inMemoryStore.getSymbols());
    }

    public SymbolResponse getSymbolResponseById(String id) {
        return symbolResponseConverter.toResponse(this.getSymbolById(id));
    }

    public Symbol getSymbolById(String id) {
        return inMemoryStore.getSymbols().stream()
                        .filter(symbol -> symbol.value().equals(id))
                        .findAny()
                        .orElse(null);
    }
}
