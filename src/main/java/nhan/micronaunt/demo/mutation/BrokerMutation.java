package nhan.micronaunt.demo.mutation;


import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.micronaut.validation.Validated;
import jakarta.validation.Valid;
import nhan.micronaunt.demo.annotation.GraphQLService;
import nhan.micronaunt.demo.request.OwnerRequest;
import nhan.micronaunt.demo.response.OwnerResponse;
import nhan.micronaunt.demo.services.OwnerService;

@GraphQLService
@Validated
public class BrokerMutation {

    private final OwnerService ownerService;
    public BrokerMutation(OwnerService ownerService) {
        this.ownerService = ownerService;
    }

    @GraphQLMutation
    @SuppressWarnings("unused")
    public OwnerResponse addNewOwner(@Valid @GraphQLArgument(name = "owner_request") OwnerRequest request) {
        return ownerService.addOwner(request);
    }

}
