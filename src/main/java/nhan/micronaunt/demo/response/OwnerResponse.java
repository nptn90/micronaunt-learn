package nhan.micronaunt.demo.response;

import nhan.micronaunt.demo.entity.Owner;

public class OwnerResponse extends BaseResponse {
    private final String name;
    private final String power;

    public OwnerResponse(Owner owner) {
        this.name = owner.name();
        this.power = owner.power();
    }

    public String getName() {
        return name;
    }

    public String getPower() {
        return power;
    }
}
