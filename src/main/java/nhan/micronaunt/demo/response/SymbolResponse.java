package nhan.micronaunt.demo.response;

import io.leangen.graphql.annotations.GraphQLIgnore;
import nhan.micronaunt.demo.entity.Symbol;

public class SymbolResponse extends BaseResponse {
    private final String value;
    private final Integer number;

    @GraphQLIgnore
    private final String notExposeField;

    @GraphQLIgnore
    private final Symbol symbol;

    public SymbolResponse(Symbol symbol) {
        this.value = symbol.value();
        this.number = symbol.number();
        this.notExposeField = symbol.notExposeField();
        this.symbol = symbol;
    }

    public String getValue() {
        return value;
    }

    public Integer getNumber() {
        return number;
    }

    public String getNotExposeField() {
        return notExposeField;
    }

    public Symbol getSymbol() {
        return symbol;
    }
}
