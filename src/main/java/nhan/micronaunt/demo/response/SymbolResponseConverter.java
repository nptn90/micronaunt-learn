package nhan.micronaunt.demo.response;

import jakarta.inject.Singleton;
import nhan.micronaunt.demo.converter.BaseResponseConverter;
import nhan.micronaunt.demo.entity.Symbol;

@Singleton
public class SymbolResponseConverter extends BaseResponseConverter<SymbolResponse, Symbol> {

    @Override
    public SymbolResponse convert(Symbol symbol) {
        if (symbol == null) {
            return null;
        }
        return new SymbolResponse(symbol);
    }
}
