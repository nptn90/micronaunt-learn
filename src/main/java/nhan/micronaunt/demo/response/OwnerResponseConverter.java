package nhan.micronaunt.demo.response;

import jakarta.inject.Singleton;
import nhan.micronaunt.demo.converter.BaseResponseConverter;
import nhan.micronaunt.demo.entity.Owner;

@Singleton
public class OwnerResponseConverter extends BaseResponseConverter<OwnerResponse, Owner> {
    @Override
    public OwnerResponse convert(Owner owner) {
        return new OwnerResponse(owner);
    }
}
