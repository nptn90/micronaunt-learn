package nhan.micronaunt.demo.request;

import io.micronaut.core.annotation.Introspected;
import jakarta.validation.constraints.NotEmpty;
import nhan.micronaunt.demo.validator.MaxStringConstraint;

@Introspected
public record OwnerRequest(
        @NotEmpty @MaxStringConstraint(maxValue = 10) String name,
        String description,
        @NotEmpty String symbolId,
        int holdNumber) {
}
