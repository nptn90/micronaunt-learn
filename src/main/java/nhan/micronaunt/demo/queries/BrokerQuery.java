package nhan.micronaunt.demo.queries;

import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLContext;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.micronaut.validation.Validated;
import jakarta.validation.constraints.NotEmpty;
import nhan.micronaunt.demo.annotation.GraphQLService;
import nhan.micronaunt.demo.entity.Symbol;
import nhan.micronaunt.demo.response.OwnerResponse;
import nhan.micronaunt.demo.response.SymbolResponse;
import nhan.micronaunt.demo.services.OwnerService;
import nhan.micronaunt.demo.services.SymbolService;
import nhan.micronaunt.demo.validator.MaxStringConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@GraphQLService
@Validated
public class BrokerQuery {

    private final Logger logger = LoggerFactory.getLogger(BrokerQuery.class);

    private final SymbolService symbolService;
    private final OwnerService ownerService;

    public BrokerQuery(SymbolService symbolService, OwnerService ownerService) {
        this.symbolService = symbolService;
        this.ownerService = ownerService;
    }

    @GraphQLQuery
    @SuppressWarnings("unused")
    public List<SymbolResponse> getSymbols() {
        return symbolService.getAllSymbolsResponse();
    }

    @GraphQLQuery
    @SuppressWarnings("unused")
    public SymbolResponse getSymbolById(@NotEmpty @MaxStringConstraint(maxValue = 4) String id) {
        return symbolService.getSymbolResponseById(id);
    }

    @GraphQLQuery
    @SuppressWarnings("unused")
    public List<OwnerResponse> getAllOwners(@GraphQLContext SymbolResponse symbolResponse) {
        Symbol symbol = symbolResponse.getSymbol();
        return ownerService.toResponseList(symbol.owners());
    }

    @GraphQLQuery
    @SuppressWarnings("unused")
    public List<OwnerResponse> getOwners(@GraphQLContext SymbolResponse symbolResponse, @NotEmpty @GraphQLArgument(name = "owner_name") String ownerName) {
        logger.debug("I'm quering");
        Symbol symbol = symbolResponse.getSymbol();
        if (symbol.owners().stream().anyMatch(owner -> owner.name().equals(ownerName))) {
            return ownerService.toResponseList(symbol.owners(), ownerName);
        }
        return List.of();
    }
}
