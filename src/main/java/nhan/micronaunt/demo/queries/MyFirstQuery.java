package nhan.micronaunt.demo.queries;

import io.leangen.graphql.annotations.GraphQLQuery;
import nhan.micronaunt.demo.annotation.GraphQLService;

@GraphQLService
public class MyFirstQuery {

    @GraphQLQuery
    public String hello() {
        return "hello world";
    }
}
