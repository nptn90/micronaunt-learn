package nhan.micronaunt.demo.store;

import com.github.javafaker.Faker;
import com.github.javafaker.Superhero;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Singleton;
import nhan.micronaunt.demo.entity.Owner;
import nhan.micronaunt.demo.entity.Symbol;

import java.util.*;
import java.util.stream.IntStream;

@Singleton
public class InMemoryStore {

    private final Map<String, Symbol> symbols = new HashMap<>();
    private final Faker faker = new Faker();

    @PostConstruct
    public void init() {
        IntStream.range(0, 10).forEach(i -> addNewSymbol());
    }

    public void addNewSymbol() {
        int numberOfStock = faker.number().randomDigit();
        List<Owner> owners = initOwners(numberOfStock);

        Symbol symbol = new Symbol(faker.stock().nsdqSymbol(), numberOfStock, "NOT EXPOSE FIELD", owners);
        symbols.put(symbol.value(), symbol);
    }

    public List<Symbol> getSymbols() {
        return new ArrayList<>(symbols.values());
    }

    public Map<String, Symbol> getMapSymbols() {
        return symbols;
    }

    private Owner initOwner() {
        Superhero superhero = faker.superhero();
        return new Owner(superhero.name(), superhero.power());
    }

    private List<Owner> initOwners(int numberOfStock) {
        List<Owner> owners = new ArrayList<>();
        int endRange = faker.number().numberBetween(0, numberOfStock);
        IntStream.range(0, endRange).forEach(i -> owners.add(initOwner()));
        return owners;
    }
}
