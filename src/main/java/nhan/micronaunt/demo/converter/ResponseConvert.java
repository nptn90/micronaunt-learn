package nhan.micronaunt.demo.converter;

public interface ResponseConvert<T, E> {
    /**
     * convert from Entity to Response
     * @return ResponseType
     */
    T convert(E e);
}
