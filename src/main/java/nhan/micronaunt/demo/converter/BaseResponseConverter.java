package nhan.micronaunt.demo.converter;

import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseResponseConverter<T, E> implements ResponseConvert<T, E> {

    public T toResponse(E e) {
        return convert(e);
    }

    public List<T> toResponseList(List<E> listEntity) {
        return listEntity
                .stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }
}
