package nhan.micronaunt.demo.entity;

import nhan.micronaunt.demo.request.OwnerRequest;

public record Owner(String name, String power) {
    public Owner(OwnerRequest ownerRequest) {
       this(ownerRequest.name(), ownerRequest.description());
    }
}
