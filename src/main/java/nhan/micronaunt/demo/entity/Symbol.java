package nhan.micronaunt.demo.entity;

import java.util.List;

/**
 * this is represent for entity class from DB
 * @param value
 * @param number
 * @param notExposeField
 * @param owners
 */
public record Symbol(String value,
                     Integer number,
                     String notExposeField,
                     List<Owner> owners) {
}
