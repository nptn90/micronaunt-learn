package nhan.micronaunt.demo.factory;

import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import jakarta.inject.Singleton;
import nhan.micronaunt.demo.bean.MyBean;

@Factory
public class HelloBeanFactory {
    @Bean
    @Singleton
    public MyBean createMyBean() {
        return new MyBean();
    }
}
