package nhan.micronaunt.demo.broker;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import nhan.micronaunt.demo.bean.MyBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller("/symbols")
public class SymbolsController {

    private final MyBean myBean;
    private static final Logger log = LoggerFactory.getLogger(SymbolsController.class);
    public SymbolsController(MyBean myBean) {
        this.myBean = myBean;
    }

    @Get
    public String hello() {
        log.debug("I'm debugging");

        return myBean.helloBean();
    }

}
